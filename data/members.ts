import { StaticImageData } from 'next/image';
import MemberData from 'autogen/members';
import MemberType from './membertype';


type Member = {
  id: string,
  type: MemberType,
  name: string,
  imageData: StaticImageData,
  website: string,
}

const Members: Member[] = MemberData;

export { type Member, Members, MemberType }
