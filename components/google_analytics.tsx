import * as React from "react";
import TagManager from 'react-gtm-module'
import { CookieConsentUpdateEvent, CookiePermissions, getCookiePermissions } from '@/components/cookie_consent'

interface IGAProps {
}

interface IGAState {
}

class GoogleAnalytics extends React.Component<IGAProps, IGAState> {
    constructor(props: IGAProps) {
        super(props);
    }

    startAnalytics() {
        let tagManagerArgs = {
            gtmId: 'GTM-WLHTXFF',
        }

        TagManager.initialize(tagManagerArgs);
    }

    componentDidMount() {
        // Listen for updates to the cookie consent
        // I would like to write this like this, but typescript doesn't like it
//        document.addEventListener(CookieConsentUpdateEvent, (e: CustomEvent<CookiePermissions>) => {
        document.addEventListener(CookieConsentUpdateEvent, (e) => {
            let event = e as CustomEvent<CookiePermissions>;

            if(event.detail.allow) {
                this.startAnalytics();
            }
        })

        // Check to see if we have already agreed to analytics
        if(getCookiePermissions()?.allow == true) {
            this.startAnalytics();
        }
    }

    public render(): JSX.Element {
        return <></>
    }
}



export default GoogleAnalytics;