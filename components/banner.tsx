import { BannerType, Images } from 'data/banners';
import Image from 'next/image'
import Link from "next/link";
import { Card, Ratio } from 'react-bootstrap';

type Props = {
    data: BannerType,
}

const Banner =  (props: Props) => {
    type ImagesKey = keyof typeof Images;
    let data=props.data;
    // TODO: Auto set background if one is not set
    let imageKey = (data?.background ?? 'banner/banner1.jpg') as ImagesKey;
    return (
        (<Link href={data.slug} passHref>
            <Card className="banner">
                <Ratio aspectRatio={400/1920}>
                    <Image className="card-img" src={Images[imageKey]} alt='banner'></Image>
                </Ratio>
                <Card.ImgOverlay>
                    <h3>{data.title}</h3>
                    <hr/>
                    <p className="card-text">{data.description}</p>
                </Card.ImgOverlay>
            </Card>
        </Link>)
    );
}

export default Banner;