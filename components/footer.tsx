import Link from "next/link";
import { Col, Container, ListGroup, Nav, NavItem, Row } from "react-bootstrap";
import Vercel from 'public/powered-by-vercel.svg'

interface MenuItem {
    name: string;
    url: string;
}

const footerMenu: MenuItem[] = [
    { name: "Accessability", url: "/about/accessability" },
    { name: "Privacy", url: "/about/privacy" },
    { name: "Terms of Use", url: "/about/terms_of_use" },
]

const menu = (items: MenuItem[]) => {
    return items.map((item, index) =>
        <Nav.Item key={index}>
            <Link href={item.url} passHref legacyBehavior>
                <Nav.Link>{item.name}</Nav.Link>
            </Link>
        </Nav.Item>
    );
}  

const Footer = () => {
    return (
        <Container>
            <hr></hr>
            <Row>
                <Col>
                    <Nav>
                        {menu(footerMenu)}
                    </Nav>
                </Col>
                {/* <Col><a href='https://vercel.com/?utm_source=soafee&utm_campaign=oss'><Vercel style={{height: '30px'}}/></a></Col> */}
                <Col style={{textAlign: 'right'}}>© 2023 SOAFEE • All Rights Reserved</Col>
            </Row>
        </Container>
    )
}

export default Footer;

