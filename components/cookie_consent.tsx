import { useEffect, useState } from 'react';
import Link from "next/link";
import { Button, Toast, ToastContainer } from "react-bootstrap";
import Cookies from 'universal-cookie'; 

const cookies = new Cookies();

export interface CookiePermissions {
    version: number;
    allow: boolean;
}


const CookieConsentName = 'cookieConsent';
export const CookieConsentUpdateEvent = 'cookieConsentUpdatedEvent'

export let getCookiePermissions = () : CookiePermissions | null => {
    let value = cookies.get(CookieConsentName);
    if(value!=undefined) {
        if(value.version==1) {
            return value;
        }
    }
    // Return the default permissions if
    return null;
}

const CookieConsent = () => {
    const [show, setShow] = useState(false);
    // Defer setting 'show' based on cookie to ensure tree is always in sync
    useEffect(() => setShow(getCookiePermissions()==null), [show]);

    let accept = (allow: boolean) => {
        // Create a date one year in the future for the cookie expiration
        var date = new Date();
        date.setTime(date.getTime() + (365*24*60*60*1000));
        // Set the cookie value
        let cookiePermissions: CookiePermissions = {
            version: 1,
            allow: allow
        } 
        cookies.set(CookieConsentName, JSON.stringify(cookiePermissions), {expires: date})
        // Hide the consent request popup
        setShow(false);

        // Inform the rest of the system if concent has been given
        let event = new CustomEvent<CookiePermissions>(CookieConsentUpdateEvent, {detail: cookiePermissions});
        document.dispatchEvent(event);
    }


    return (
        <ToastContainer className="fixed-bottom p-4">
            <Toast bg='dark' className='w-100' show={show}>
                <Toast.Header closeButton={false}>
                    <strong>Cookie Warning</strong>
                </Toast.Header>
                <Toast.Body className='text-white'>
                    This website stores data such as cookies to enable site functionality including analytics and personalization. By using this website, you automatically accept that we use cookies. 
                    For more information check out our <Link href='/about/privacy'>Cookie and Privacy Policy</Link>
                    <hr/>
                    <Button onClick={() => accept(false)} variant='light'>Deny</Button>
                    <Button onClick={() => accept(true)}>Accept</Button>
                </Toast.Body>
            </Toast>
        </ToastContainer>
    );
}

export default CookieConsent;