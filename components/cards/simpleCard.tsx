import { Card } from "react-bootstrap";
import OptionalCardLink from "./optionalcardlink";

type Props = {
    footer: React.ReactNode
    url?: string
    children: React.ReactNode
}


let SimpleCard = (props: Props) => {
    return <Card className="simpleCard cardHover">
        <OptionalCardLink url={props.url}>
        <Card.Body>
            {props.children}
        </Card.Body>
        <Card.Footer>{props.footer}</Card.Footer>
        </OptionalCardLink>
    </Card>
}

export default SimpleCard;