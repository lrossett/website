import Link from "next/link"

type Props = {
    url?: string,
    children: React.ReactNode
}

let OptionalCardLink = (props: Props) => {
    if(props.url) {
        return (
            (<Link
                href={props.url}
                passHref
                style={{textDecoration: "none", color: "inherit"}}>
                {props.children}
            </Link>)
        );
    } else {
        return <>{props.children}</>
    }
}

export default OptionalCardLink