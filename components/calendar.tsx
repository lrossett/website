
type Props = {
    src: string,
    color?: string,
    bgcolor?: string,
    height?: number,
}

const Calendar = (props: Props) => {
    let url = new URL('https://calendar.google.com/calendar/embed');
    url.searchParams.set('height', '600');
    url.searchParams.set('wkst', '1');
    url.searchParams.set('bgcolor', props?.bgcolor ?? "#ffffff");
    url.searchParams.set('showTitle', '1');
    url.searchParams.set('src', props.src);
    url.searchParams.set('color', props?.color ?? "#3F51B5");

    return <iframe src={url.toString()} style={{border: 'solid 1px #777'}} width="100%" height={props?.height ?? 600} scrolling="no"></iframe>
}

export default Calendar;