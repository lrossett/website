import Settings from 'fast-glob/out/settings';
import SOAFEE from '/public//soafee.svg'

type Props = {
    width?: number|string;
    height?: number|string;
    allowAnimate?: boolean;
    style?: string;
    alt?: string;
}


const SOAFEELogo = (props: Props) => {
    let mouseEnter = (e: Event) => {
        if(props?.allowAnimate) {
            (e.target as Element).classList.add("animate");
        }
    }

    let mouseLeave = (e: Event) => {
        if(props?.allowAnimate) {
            (e.target as Element).classList.remove("animate");
        }
    }

    return <SOAFEE className="soafee-logo" alt={props.alt||""} style={props.style} width={props.width||undefined} height={props.height||undefined} onMouseEnter={mouseEnter} onMouseLeave={mouseLeave}/>
}

export default SOAFEELogo