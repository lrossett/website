import Head from "next/head"
import Footer from "./footer"
import Header from "./header"
import { Container, Ratio } from "react-bootstrap"
import MemberLogo from "./memberlogo"
import CookieConsent from "./cookie_consent"
import { Member } from 'data/members'


type FrontMatter = {
  layout?: string,
  title?: string,
  description?: string,
  member?: Member,
  memberId?: string,
}

type Props = {
    frontmatter?: FrontMatter,
    preview?: boolean
    children: React.ReactNode
  }
  
const MemberPage = (props?: Props) => {
  let id = props?.frontmatter?.member || props?.frontmatter?.memberId;

  return (
    <>
      <Ratio aspectRatio={'16x9'} className='justify-content-center' style={{width: '30%'}}>
        <MemberLogo member={id} />
      </Ratio>
      {props?.children}
    </>
  )
}

const DefaultPage = (props?: Props) => {
  return props?.children
}



  //const Layout = ({ preview, children}: Props) => {
const Layout = (props?: Props) => {
  let layout = props?.frontmatter?.layout;

  let generateContent = (props?: Props) => {

    let layout = props?.frontmatter?.layout;
    if(layout==undefined || layout=='default') {
      return DefaultPage(props)
    } else if (layout == 'memberPage') {
      return MemberPage(props)
    }
    // Force a compilation error, but use @ts-ignore to stop this being
    // a error in your editor
    // @ts-ignore
    error
  }


  return (
    <>
      <Head>
        <title>{props?.frontmatter?.title}</title>
        <meta name="description" content={props?.frontmatter?.description} />
        <link rel="icon" type="image/svg+xml" href="/favicon.svg" />
      </Head>

      <Header />

      <main>
          <Container>{generateContent(props)}</Container>
      </main>

      <Footer />
      <CookieConsent></CookieConsent>
    </>
  )
}

export default Layout;